/**
 *
 * @copyright 2019 Extend Applications Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 */
export declare enum DIRECTION {
    OUTGOING = 1,
    INCOMING = 2
}
export declare class RAMP_Document_Type {
    static RECORD_TYPE: string;
    static FIELDS: any;
    static getDocumentType(id: number): IRAMP_Document_Type;
    static getList(): IRAMP_Document_Type[];
}
export declare type DocumentTypeScriptId = 'ramp_document_sku' | 'ramp_document_receipt_request' | 'ramp_document_ship_request' | 'ramp_document_ship_confirm' | 'ramp_document_receipt_confirm' | 'ramp_document_item_balance' | 'ramp_document_item_adjustement';
export interface IRAMP_Document_Type {
    id: number;
    name: string;
    direction: number;
    scriptid: DocumentTypeScriptId;
}
