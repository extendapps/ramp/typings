/**
 * @copyright 2019 Extend Applications Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
import * as search from 'N/search';
import { DocumentTypeScriptId } from './RAMP_Document_Type';
export declare class RAMP {
    private static instance;
    private companyCode;
    private warehouseCode;
    constructor(options: RAMPConstructorOptions);
    readonly CompanyCode: string;
    readonly WarehousCode: string;
    static getInstance(options: RAMPConstructorOptions): RAMP;
    sendDocument(options: SendDocumentOptions): void;
    readDocument(options: any): void;
    private static findXSD;
    createDocument(options: CreateDocumentOptions): void;
}
export interface FindXSDOptions {
    xsdName: string;
    Found: (fileId: number) => void;
    NotFound: () => void;
}
export interface RAMPConstructorOptions {
    id: number;
}
export interface SendDocumentOptions {
    fileId: number;
    Sent?: () => void;
    Failed?: () => void;
}
export interface CreateDocumentOptions {
    documentTypeScriptId: DocumentTypeScriptId;
    tranSearch: search.Search;
    Created?: (fileId: number) => void;
    Failed?: () => void;
}
