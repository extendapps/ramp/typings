/**
 *
 * @copyright 2019 Extend Apps Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */
export declare let sendDocument: ISendDocument;
interface ISendDocument {
    (options: SendDocumentOptions): any;
}
interface SendDocumentOptions {
    fileId: number;
    Sent?: () => void;
    Failed?: () => void;
}
export interface SendDocumentPlugIn {
    sendDocument: ISendDocument;
}
export {};
