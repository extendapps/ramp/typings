/**
 *
 * @copyright 2019 Extend Apps Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */
import { OutboundOrder } from './Documents/OutboundOrder';
import { RAMP } from './RAMP';
import * as search from 'N/search';
import { ExpectedReceipt } from './Documents/ExpectedReceipt';
export declare let createOutboundOrder: ICreateOutboundOrderDocument;
export declare let createExpectedReceipt: ICreateExpectedReceiptDocument;
interface ICreateOutboundOrderDocument {
    (tranSearch: search.Search, ramp: RAMP): OutboundOrder;
}
interface ICreateExpectedReceiptDocument {
    (tranSearch: search.Search, ramp: RAMP): ExpectedReceipt;
}
export interface RAMPDocumentPlugIn {
    createOutboundOrder: ICreateOutboundOrderDocument;
    createExpectedReceipt: ICreateExpectedReceiptDocument;
    createProductMaster: any;
}
export {};
