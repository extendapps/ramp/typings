import { ACTION, Address, SKU } from './Common';
export interface Carrier {
    Carrier: string;
    Service: string;
}
export interface Customer {
    Company: string;
    CustomerAddress: Address;
    Customer: string;
    FreightBillTo: string;
    ShipTo: string;
    ShipToAddress: Address;
}
export interface MarkForAddress {
    Action: ACTION;
    Address1: string;
    City: string;
    Country: string;
    Name: string;
    PhoneNum: string;
    PostalCode: string;
    State: string;
}
export interface ShipmentDetail {
    Action: ACTION;
    UserDef4: string;
    ErpOrder: string;
    ErpOrderLineNum: string;
    MarkForAddress?: MarkForAddress;
    SKU: SKU;
}
export interface Details {
    ShipmentDetail: ShipmentDetail[];
}
export interface Shipment {
    Action: ACTION;
    UserDef1: string;
    UserDef2: string;
    UserDef3: string;
    UserDef4: string;
    UserDef5: string;
    UserDef6: string;
    Carrier: Carrier;
    Customer: Customer;
    ErpOrder: string;
    FreightTerms: string;
    OrderDate: string;
    OrderType: string;
    PlannedShipDate: string;
    Priority: string;
    RequestedDeliveryDate: string;
    RequestedDeliveryType: string;
    ScheduledShipDate: string;
    ShipmentId: string;
    UserDef11: string;
    UserDef13: string;
    UserDef14: string;
    Warehouse: string;
    Details: Details;
}
export interface Shipments {
    Shipment: Shipment;
}
export interface OutboundOrder {
    Shipments: Shipments;
}
