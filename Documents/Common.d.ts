export declare type ACTION = 'SAVE';
export interface Address {
    Action: ACTION;
    Address1: string;
    Address2: string;
    Address3: string;
    AttentionTo: string;
    City: string;
    Consignee: string;
    Country: string;
    EmailAddress: string;
    FaxNum: string;
    Name: string;
    PhoneNum: string;
    PostalCode: string;
    ResidentialFlag: string;
    State: string;
}
export interface SKU {
    Company: string;
    Desc: string;
    Item: string;
    NetPrice: string;
    Quantity: string;
    QuantityUm: string;
    Whs: string;
}
