import { ACTION, Address, SKU } from './Common';
export interface Vendor {
    ShipFrom: string;
    ShipFromAddress: Address;
}
export interface ReceiptDetail {
    Action: ACTION;
    CustomerOrderNum?: string;
    ErpOrderLineNum: string;
    ErpOrderNum: string;
    SKU?: SKU;
}
export interface ReceiptDetails {
    ReceiptDetail: ReceiptDetail[];
}
export interface ReceiptContainer {
    Action: ACTION;
    ContainerId: string;
    ContainerType: string;
    Qty: string;
    QtyUm: string;
    ReceiptDetail: ReceiptDetail;
}
export interface ReceiptContainers {
    ReceiptContainer: ReceiptContainer[];
}
export interface Receipt {
    Action: ACTION;
    Company: string;
    ReceiptDate: string;
    ReceiptId: string;
    ReceiptIdType: 'po' | 'rma';
    ScheduledDate: string;
    TrailerId: string;
    Vendor: Vendor;
    Warehouse: string;
    Details: ReceiptDetails;
    ReceiptContainers?: ReceiptContainers;
}
export interface PurchaseOrderDetail {
    Action: ACTION;
    LineNumber: number;
    SKU: SKU;
}
export interface PODetails {
    PurchaseOrderDetail: PurchaseOrderDetail[];
}
export interface PurchaseOrder {
    Action: ACTION;
    UserDef1: string;
    Company: string;
    PurchaseOrderId: string;
    ReceiptType: 'PO';
    Warehouse: string;
    Details: PODetails;
}
export interface Receipts {
    Receipt?: Receipt;
    PurchaseOrder?: PurchaseOrder;
}
export interface WMWDATA {
    Receipts: Receipts;
}
export interface WMWROOT {
    WMWDATA: WMWDATA;
    _xmlns: 'http://www.manh.com/ILSNET/Interface';
}
export interface ExpectedReceipt {
    WMWROOT: WMWROOT;
}
